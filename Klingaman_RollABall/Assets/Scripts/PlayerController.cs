﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    public Material playerMaterial;

    //float variables to assign RGB values
    public float redColor;
    public float blueColor;
    public float greenColor;


    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    // Start is called before the first frame update
    void Start()
    {
        //Allows us to access the Rigidbody component of the player
        rb = GetComponent<Rigidbody>();
        
        //Allows us to access the material within the mesh renderer
        playerMaterial = GetComponent<Renderer>().material;

        //Initializes count to 0
        count = 0;

        //Starts the SetCountText function to display the amount of items picked up
        SetCountText();

        //Initializes the win text to be hidden
        winTextObject.SetActive(false);
    }

    
    // Gets the movement direction that the user inputs and moves the player using their x and y positions
    void OnMove(InputValue movementValue)
    {

        Vector2 movementVector = movementValue.Get<Vector2>();

        //Assigns the variable to the x position of the movement vector
        movementX = movementVector.x;

        //Assigns the variable to the y position of the movement vector
        movementY = movementVector.y;
    }

    /* Sets the count text on the screen to track collection progress
     * and if all objects are collected, then it enables the win text
     */
    void SetCountText()
    {
        //Sets the countText's text
        countText.text = "Count: " + count.ToString();

        //Checks if all of the pickups have been collected
        if(count >= 12)
        {
            //If all objects are collected, then the win text gets enabled
            winTextObject.SetActive(true);
            
        }
    }

    private void FixedUpdate()
    {
        // Assigns movement to be a new Vector3 to calculate it's direction when being moved
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        // Adds force to the player to make it move
        rb.AddForce(movement * speed);
    }
    
    
    private void OnTriggerEnter(Collider other)
    {
        /* Assigning the float variables redColor, blueColor, and greenColor
         * to a random value between 0 and 1. 
         */
        redColor = Random.Range(0f, 1f);
        blueColor = Random.Range(0f, 1f);
        greenColor = Random.Range(0f, 1f);

        //Checks the tag of the gameObject to decide if it is a PickUp object
        if (other.gameObject.CompareTag("PickUp"))
        {
            //Deletes the gameObject from the scene after collision
            other.gameObject.SetActive(false);

            //Increases the count variable by 1 everytime an item is picked up
            count += 1;

            //Sets the color of the material attached to the player to a randomized color
            playerMaterial.color = new Color(redColor, greenColor, blueColor);

            //Calls the SetCountText function to see the amount of items that are picked up
            SetCountText();
        }
        
    }
}
