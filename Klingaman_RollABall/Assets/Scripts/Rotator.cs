﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    /* This script is used for rotating the
     * PickUp items that appear on the board.
     * 
     */ 

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime); // Rotates the PickUps around the x, y, and z axis to make them spin
    }
}
