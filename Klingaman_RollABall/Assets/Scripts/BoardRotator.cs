﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardRotator : MonoBehaviour
{
    /* This script is used to rotate everything on the board
     * simultaneously except for the player. Everything rotates
     * around the y-axis by 10.
     */
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 10, 0) * Time.deltaTime); // Rotates the position of the whole board around the y-axis by 10
    }
}
